import FacebookIcon from '@mui/icons-material/Facebook';
import InstagramIcon from '@mui/icons-material/Instagram';
import LinkedInIcon from '@mui/icons-material/LinkedIn';
import TwitterIcon from '@mui/icons-material/Twitter';
import { Box, Stack, Typography } from '@mui/material';
import React from 'react';
import styled from 'styled-components';
const Footer = () => {
  return (
    <Box width={'90%'} margin={'auto'}>
      <Stack
        direction={'row'}
        justifyContent={'space-between'}
        width={'100%'}
        my={2}
      >
        <Typography>© 2023 IQTEST.AI</Typography>
        <Stack direction={'row'} gap={'1rem'} alignItems={'center'}>
          <StyledIcon>
            <InstagramIcon fontSize='small' />{' '}
          </StyledIcon>
          <StyledIcon>
            <FacebookIcon fontSize='small' />{' '}
          </StyledIcon>
          <StyledIcon>
            <TwitterIcon fontSize='small' />{' '}
          </StyledIcon>
          <StyledIcon>
            <LinkedInIcon fontSize='small' />{' '}
          </StyledIcon>
        </Stack>
      </Stack>
    </Box>
  );
};

export default Footer;

const StyledIcon = styled(Box)(({ theme }) => ({
  borderRadius: '50%',
  backgroundColor: '#275D71',
  height: '35px',
  width: '35px',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  color: 'white'
}));
