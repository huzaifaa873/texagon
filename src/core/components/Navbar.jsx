// import MenuIcon from '@mui/icons-material/Menu';
import { Stack } from '@mui/material';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
// import IconButton from '@mui/material/IconButton';
import FormControl from '@mui/material/FormControl';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import * as React from 'react';
import Logo from '../custom-icons/Logo';
const Navbar = () => {
  const [age, setAge] = React.useState(1);

  const handleChange = (event) => {
    setAge(event.target.value);
  };

  return (
    <Box
      sx={{
        flexGrow: 1,
        backgroundColor: '#fff'
      }}
    >
      <AppBar
        position='static'
        sx={{
          backgroundColor: '#fff',
          color: '#000'
        }}
      >
        <Toolbar
          sx={{
            justifyContent: 'space-between'
          }}
        >
          {/* <IconButton
            size='large'
            edge='start'
            color='inherit'
            aria-label='menu'
            sx={{ mr: 2 }}
          >
            <MenuIcon />
          </IconButton> */}
          <Stack direction={'row'} alignItems={'center'} gap={'.5rem'}>
            <Logo />
            <Typography variant='h6' component='div' sx={{ flexGrow: 1 }}>
              IQTEST.ai
            </Typography>
          </Stack>
          <Stack direction={'row'} gap={'1rem'}>
            <FormControl
              size='small'
              fullWidth
              sx={{
                border: 'none',
                minWidth: '120px',
                width: '100%',
                backgroundColor: '#EDF2F7',
                '& .MuiInputBase-root:focus-visible': {
                  outline: 'none',
                  border: 'none'
                }
              }}
            >
              <Select value={age} label='Age' onChange={handleChange}>
                <MenuItem value={1}>English</MenuItem>
                <MenuItem value={2}>Urdu</MenuItem>
                <MenuItem value={3}>French</MenuItem>
              </Select>
            </FormControl>
            <Button
              variant='contained'
              size='small'
              sx={{
                borderRadius: '24px',
                padding: '6px 20px'
              }}
            >
              Login
            </Button>
          </Stack>
        </Toolbar>
      </AppBar>
    </Box>
  );
};

export default Navbar;
