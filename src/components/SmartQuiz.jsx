import EmailIcon from '@mui/icons-material/Email';
import {
  Box,
  Button,
  Divider,
  Grid,
  List,
  ListItem,
  ListItemText,
  Paper,
  Stack,
  TextField,
  Typography
} from '@mui/material';
import React from 'react';
import styled from 'styled-components';
import Logo from '../core/custom-icons/Logo';
const SmartQuiz = () => {
  return (
    <Box width={'90%'} margin={'auto'}>
      <Stack direction={'row'} justifyContent={'center'} my={2}>
        <Typography variant='h1' component={'span'}>
          Smart Quiz
          <Typography variant='h1' component={'span'} color={'#5235E8'} mx={2}>
            Short
          </Typography>
          Answer
        </Typography>
      </Stack>

      <Paper
        elevation={1}
        sx={{
          backgroundColor: '#000',
          color: '#fff ',
          padding: '30px',
          borderRadius: '24px'
        }}
      >
        <Grid container spacing={2} marginY={2}>
          <Grid item xs={12} md={12} lg={6}>
            <Stack direction={'column'} gap={'2rem'} maxWidth={'500px'}>
              <StyledIcon>
                <EmailIcon fontSize='large' />
              </StyledIcon>
              <Typography variant='h2'>Keep up with the latest</Typography>
              <Typography variant='h5'>
                Join our newsletter to stay up to date on features and releases.
              </Typography>
            </Stack>
          </Grid>
          <Grid item xs={12} md={12} lg={6}>
            <Stack
              height={'100%'}
              direction={'column'}
              gap={'1rem'}
              alignItems={'center'}
              justifyContent={'center'}
            >
              <Typography variant='h5'>Stay up to date</Typography>
              <Stack
                direction={'row'}
                alignItems={'center'}
                gap={'1rem'}
                width={'100%'}
                justifyContent={'center'}
                flexWrap={'wrap'}
              >
                <TextField
                  label='Email'
                  type='text'
                  variant='filled'
                  placeholder='Enter your email'
                  sx={{
                    backgroundColor: '#EDF2F7',
                    borderRadius: '28px',
                    minWidth: '300px',
                    '& .css-batk84-MuiInputBase-root-MuiFilledInput-root::after':
                      {
                        borderBottom: 'none'
                      }
                  }}
                />
                <Button
                  variant='contained'
                  size='large'
                  sx={{
                    borderRadius: '24px',
                    padding: '14px 18px'
                  }}
                >
                  Subscribe
                </Button>
              </Stack>
              <Typography variant='p'>
                By subscribing you agree with our Privacy Policy
              </Typography>
            </Stack>
          </Grid>
          <Grid item xs={12}>
            <Divider
              sx={{
                borderColor: '#fff',
                margin: '50px 0px'
              }}
            />
          </Grid>
          <Grid item xs={12} md={12} lg={6}>
            <Stack direction={'column'} gap={'1rem'} maxWidth={'400px'}>
              <Stack direction={'row'} alignItems={'center'} gap={'.5rem'}>
                <Logo />
                <Typography variant='h5'>IQTEST.ai</Typography>
              </Stack>
              <Typography
                variant='p'
                sx={{
                  color: '#999'
                }}
              >
                Test your IQ in a very easy and simple step not complex.
              </Typography>
            </Stack>
          </Grid>
          <Grid item xs={12} md={12} lg={6}>
            <Stack
              direction={'row'}
              gap={'2rem'}
              width={'100%'}
              justifyContent={'center'}
              flexWrap={'wrap'}
            >
              <Stack direction={'column'}>
                <Typography variant='p' component='div' pl={2}>
                  IQTEST
                </Typography>

                <List
                  sx={{
                    color: '#999'
                  }}
                >
                  <ListItem>
                    <ListItemText primary='Quiz' />
                  </ListItem>
                  <ListItem>
                    <ListItemText primary='Results' />
                  </ListItem>{' '}
                  <ListItem>
                    <ListItemText primary='Pricing' />
                  </ListItem>{' '}
                </List>
              </Stack>
              <Stack direction={'column'}>
                <Typography variant='p' component='div' pl={2}>
                  Text only
                </Typography>

                <List
                  sx={{
                    color: '#999'
                  }}
                >
                  <ListItem>
                    <ListItemText primary='Quiz' />
                  </ListItem>
                  <ListItem>
                    <ListItemText primary='Results' />
                  </ListItem>{' '}
                  <ListItem>
                    <ListItemText primary='Pricing' />
                  </ListItem>{' '}
                </List>
              </Stack>
              <Stack direction={'column'}>
                <Typography variant='p' component='div' pl={2}>
                  Text only
                </Typography>

                <List
                  sx={{
                    color: '#999'
                  }}
                >
                  <ListItem>
                    <ListItemText primary='Quiz' />
                  </ListItem>
                  <ListItem>
                    <ListItemText primary='Results' />
                  </ListItem>{' '}
                  <ListItem>
                    <ListItemText primary='Pricing' />
                  </ListItem>{' '}
                </List>
              </Stack>
              <Stack direction={'column'}>
                <Typography variant='p' component='div' pl={2}>
                  Text only
                </Typography>

                <List
                  sx={{
                    color: '#999'
                  }}
                >
                  <ListItem>
                    <ListItemText primary='Quiz' />
                  </ListItem>
                  <ListItem>
                    <ListItemText primary='Results' />
                  </ListItem>{' '}
                  <ListItem>
                    <ListItemText primary='Pricing' />
                  </ListItem>{' '}
                </List>
              </Stack>
            </Stack>
          </Grid>
        </Grid>
      </Paper>
    </Box>
  );
};

export default SmartQuiz;

const StyledIcon = styled(Box)(({ theme }) => ({
  borderRadius: '50%',
  backgroundColor: '#275D71',
  height: '70px',
  width: '70px',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  color: 'white'
}));
