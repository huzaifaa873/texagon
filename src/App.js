import './App.css';
import SmartQuiz from './components/SmartQuiz';
import Footer from './core/components/Footer';
import Navbar from './core/components/Navbar';

function App() {
  return (
    <>
      <Navbar />
      <SmartQuiz />
      <Footer />
    </>
  );
}

export default App;
